package ac.cr.ucenfotec.bl;

public class Animal {

    private String nombre;
    private String pelaje;

    public Animal() {
    }

    public Animal(String nombre, String pelaje) {
        this.nombre = nombre;
        this.pelaje = pelaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPelaje() {
        return pelaje;
    }

    public void setPelaje(String pelaje) {
        this.pelaje = pelaje;
    }

    public String toString() {
        return "Animal{" +
                "nombre='" + nombre + '\'' +
                ", pelaje='" + pelaje + '\'' +
                '}';
    }
}
