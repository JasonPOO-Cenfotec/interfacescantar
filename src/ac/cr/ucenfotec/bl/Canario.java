package ac.cr.ucenfotec.bl;

public class Canario extends Animal implements ICantable,IHabitat{

    private int edad;

    public Canario() {
    }

    public Canario(String nombre, String pelaje, int edad) {
        super(nombre, pelaje);
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return super.toString() + "Canario{" + "edad=" + edad + '}';
    }

    @Override
    public String cantar(int tiempo) {
        return super.getNombre() + " canta riririr durante " + tiempo+ " horas!";
    }

    @Override
    public String mostrarHabitat() {
        return super.getNombre() + ", vive en los arboles!";
    }

    @Override
    public String dondeVive() {
        return super.getNombre() + ", vive en la Sabana!";
    }
}
