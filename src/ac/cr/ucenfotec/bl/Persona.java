package ac.cr.ucenfotec.bl;

public class Persona implements ICantable,IHogar{

    private String identificacion;
    private String nombre;

    public Persona() {
    }

    public Persona(String identificacion, String nombre) {
        this.identificacion = identificacion;
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String toString() {
        return "Persona{" +
                "identificacion='" + identificacion + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    @Override
    public String cantar(int tiempo) {
        return nombre + " canta Opera durante "+ tiempo+ " horas!";
    }

    @Override
    public String dondeVive() {
        return nombre + ", vive en San José centro.";
    }
}
