package ac.cr.ucenfotec.bl;

public interface IHabitat extends IHogar{
    public abstract String mostrarHabitat();
}
