package ac.cr.ucenfotec.ui;

import ac.cr.ucenfotec.bl.Canario;
import ac.cr.ucenfotec.bl.ICantable;
import ac.cr.ucenfotec.bl.Persona;
import java.util.ArrayList;

public class UI {

    public static void main(String[] args) {

        Persona persona = new Persona("123", "Marcos");
        Canario canario = new Canario("Piolín","plumas", 23);

        ArrayList<ICantable> cantantes = new ArrayList<>();
        cantantes.add(persona);
        cantantes.add(canario);

        System.out.println(persona.cantar(5));
        //System.out.println(persona.dondeVive());

        System.out.println(canario.cantar(15));
        //System.out.println(canario.dondeVive());
        //System.out.println(canario.mostrarHabitat());

        ICantable cantante = new Canario("1","pluma",45);
        System.out.println(cantante.cantar(10));


    }

}
